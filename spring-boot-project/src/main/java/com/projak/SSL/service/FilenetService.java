package com.projak.SSL.service;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

public interface FilenetService {
	
	public HttpServletResponse authenticate(String userid, String password, HttpServletResponse response);

	JSONArray generateDocumentUrl(String larNo);

}
