package com.projak.SSL.service.impl;

import java.io.IOException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.Subject;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.RepositoryRowSet;
import com.filenet.api.core.Domain;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.query.RepositoryRow;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;
import com.projak.SSL.service.FilenetService;
import com.projak.SSL.util.CEConnection;

@Service
public class FilenetServiceImpl implements FilenetService {

	@Autowired
	private Environment env;

	@Override
	public HttpServletResponse authenticate(String userid, String password, HttpServletResponse response) {
		disableSslVerification();
		try {
			String stringURL = env.getProperty("url") + "logon.do/?userid=" + userid + "&password=" + password;
			URL url = new URL(stringURL);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setConnectTimeout(5000);
			con.setReadTimeout(5000);
			con.connect();
			Map<String, List<String>> headers = con.getHeaderFields();
			Iterator<String> itr = headers.keySet().iterator();
			while (itr.hasNext()) {
				String header = itr.next();
				if (header != null && header.equalsIgnoreCase("Set-Cookie")) {
					List<String> cookies = headers.get(header);
					for (String httpCookie : cookies) {
						System.out.println("Setting Cookies : " + httpCookie + "; SameSite=None");
						response.addHeader("Set-Cookie", httpCookie + "; SameSite=None");
					}
				}
			}
			IOUtils.copy(con.getInputStream(), response.getWriter(), "UTF-8");
			con.disconnect();
		} catch (Exception ex) {
			ex.printStackTrace();
			try {
				response.sendError(500);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Exception on Adding Cookies : " + ex.fillInStackTrace());
		}

		return response;
	}

	@Override
	public JSONArray generateDocumentUrl(String applicationNo) {
		disableSslVerification();
		CEConnection ce = new CEConnection();
		Subject sub = ce.establishConnection(env.getProperty("userId"), env.getProperty("password"),
				env.getProperty("stanza"), env.getProperty("uri"));
		UserContext.get().pushSubject(sub);
		JSONArray array = new JSONArray();
		try {
			String SQL = "SELECT [This], [Id], [DocumentTitle] FROM [Forms] WHERE [ApplicationNumber] = '"
					+ applicationNo + "'";
			System.out.println(env.getProperty("ObjectStore"));
			ObjectStore OS = ce.fetchOS(env.getProperty("ObjectStore"));
			SearchScope scope = new SearchScope(OS);
			SearchSQL sql = new SearchSQL(SQL);
			RepositoryRowSet rowSet = scope.fetchRows(sql, null, null, null);

			if (!rowSet.isEmpty()) {
				Iterator iter = rowSet.iterator();
				while (iter.hasNext()) {
					RepositoryRow row = (RepositoryRow) iter.next();
					Id ID = row.getProperties().getIdValue("Id");
					String docTitle = row.getProperties().getStringValue("DocumentTitle");
					String URL = env.getProperty("documentUrl") + "desktop=" + env.getProperty("desktop")
							+ "&repositoryId="+ env.getProperty("repositoryId") + "&repositoryType="
							+ env.getProperty("repositoryType") + "&docid=" + ID.toString();
					JSONObject jobj = new JSONObject();
					jobj.put("Document Title", docTitle);
					jobj.put("Link", URL);
					array.put(jobj);
				}
			}
			System.out.println(array.toString());
			return array;
		} catch (Exception ex) {
			ex.fillInStackTrace();
			System.out.println(ex.fillInStackTrace());
			return array;
		} finally {
			UserContext.get().popSubject();

		}
	}

	private void disableSslVerification() {
		try {
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
}
