package com.projak.SSL.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.projak.SSL.service.FilenetService;
import com.projak.SSL.service.impl.FilenetServiceImpl;

@RestController
@RequestMapping("/filenet")
public class FilenetController {

	@Autowired
	private FilenetService filenetServiceImpl;

	@GetMapping(value = "/authenticate")
	public void authentication(@RequestParam(name = "username") String userName,
			@RequestParam(name = "password") String password, HttpServletResponse response,
			HttpServletRequest request) {
		filenetServiceImpl.authenticate(userName, password, response);
		System.out.println("Logon.do Called");
	}

	@GetMapping(value = "/docurl", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> docURL(@RequestParam(name = "applicationNo") String applicationNo) {
		JSONArray jarray = filenetServiceImpl.generateDocumentUrl(applicationNo);
		if (jarray == null) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(jarray.toString(), HttpStatus.OK);
		}

	}

}
