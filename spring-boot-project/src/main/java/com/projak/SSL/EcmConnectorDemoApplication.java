package com.projak.SSL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcmConnectorDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcmConnectorDemoApplication.class, args);
	}

}
