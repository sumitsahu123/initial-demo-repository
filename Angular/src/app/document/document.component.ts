import { Component, OnInit, Input } from "@angular/core";
import { Document } from "./document";
import { DocumentService } from "./document.service";
import { DocumentListComponent } from "./document-list/document-list.component";
import { Observable } from "rxjs";
import { HttpHeaderResponse } from "@angular/common/http";
import { Route, Router } from "@angular/router";

@Component({
  selector: "app-document",
  templateUrl: "./document.component.html",
  styleUrls: [
    "./document.component.css",
    "./../../../node_modules/font-awesome/css/font-awesome.css"
  ]
})
export class DocumentComponent implements OnInit {
  document: Document = new Document();
  documents: Observable<Document[]>;
  parentSelected: number;
  loggedInUser : string;

  constructor(
    private documentService: DocumentService,
    private router: Router
  ) {}


  ngOnInit() {
    // var generalDiv = document.getElementsByClassName("General-div").item(0);
    // generalDiv.addEventListener('click',function(event){
    //   console.log(event);
    //   // generalDiv.setAttribute("data-toggle")
    // });
  }

}
