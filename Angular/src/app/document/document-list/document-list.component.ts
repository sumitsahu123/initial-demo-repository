import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { DocumentService } from "../document.service";
import { Document } from "../document";
import { Router } from "@angular/router";
import { LoginService } from "src/app/login/login.service";

@Component({
  selector: "app-document-list",
  templateUrl: "./document-list.component.html",
  styleUrls: [
    "./document-list.component.css",
    "./../../../../node_modules/font-awesome/css/font-awesome.css"
  ]
})
export class DocumentListComponent implements OnInit {
  //@Input() document: Document;
  documents: Array<Document>;
  newDocument: Document = new Document();
  data: any[];
  docData: any[];
  doc: Document[] = [];
  mfActivePage: number;
  mfRowsOnPage: number;
  mfSortBy: String;
  appNo: string;
  constructor(
    private documentService: DocumentService,
    private logonService: LoginService,
    private router: Router
  ) {}


  ngOnInit() {
    if(localStorage.getItem("username") !== null){
      this.router.navigate(["/documentList"]);
    }else{
      this.router.navigate(["/"]);
    }
  }

  goBack(){
    this.router.navigate(["/entry"]);
  }


  getDocumentLink(){
    this.documentService.getDocumentLink(this.appNo).subscribe(
      data => {
        this.documents = data;
      });
  }

  openLink(link : string){
    window.open(link,"_blank");
  }

  /*deleteCustomers() {
    this.customerService.deleteAll()
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log('ERROR: ' + error));
  }


  getActualDocumentList(){

    var j=0,len= 0;
     this.documents.forEach(dos => {
      len++;
    });

    for( var i=0;i<len;i++){
      if(this.documents[i].id!=null){
        this.doc[j].id = this.documents[i].id;
        this.doc[j].documentName = this.documents[i].documentName;
        this.doc[j].parentId = this.documents[i].parentId;
        this.doc[j++].isActive = this.documents[i].isActive;
      } else {
        this.documents = this.documents[--i].children;
        len = 0;
        this.documents.forEach(dos => {
          len++;
        });
        this.doc[j].id = this.documents[i].id;
        this.doc[j].documentName = this.documents[i].documentName;
        this.doc[j].parentId = this.documents[i].parentId;
        this.doc[j++].isActive = this.documents[i].isActive;

      }
    }


  }
*/
}

export interface ParentDocument {
  id: bigint;
  documentName: string;
}
