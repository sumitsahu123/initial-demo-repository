import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable }  from "rxjs";
import { Document } from "./document"

@Injectable({
  providedIn: "root"
})
export class DocumentService {
 
  private baseUrl = "http://localhost:8081/document";
  private headers = new Headers({'Content-Type':'application/json'});
  
  private document = new Document();

  constructor(private http: HttpClient) {}

  getDocumentLink(appNo : string): Observable<any> {
    return this.http.get(window.location.origin+"/ssl/filenet/docurl/?applicationNo="+appNo);
  }

}
