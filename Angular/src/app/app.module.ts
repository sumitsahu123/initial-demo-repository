import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Router, Routes, RouterModule } from "@angular/router";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { HttpClientModule , HTTP_INTERCEPTORS } from '@angular/common/http';
import { BasicAuthHtppInterceptorService } from './login/BasicAuthHtppInterceptorService.service';
import { EntryComponent } from './entry/entry.component';
import { AuthGaurdService } from './login/AuthGaurdService.service';
import { DocumentComponent } from './document/document.component';
import { DocumentListComponent } from './document/document-list/document-list.component';
import { DataTableModule } from '@pascalhonegger/ng-datatable';
import { AngularDualListBoxModule } from "angular-dual-listbox";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { DatePipe } from "@angular/common";
import { MyLoaderComponent } from './loader/my-loader/my-loader.component';
import { LoaderService } from './services/loader.service';
import { LoaderInterceptorService } from './interceptors/loader-interceptor.service';

const appRoutes: Routes = [
  {
    path: "addDocument",
    component: DocumentComponent,
    canActivate: [AuthGaurdService]
  },
  {
    path: "documentList",
    component: DocumentListComponent,
    canActivate: [AuthGaurdService]
  },
  {
    path: "",
    component: LoginComponent

  },
 
    {// added on 28-01-2020 by sumit for landing page
      path: "entry",
      component: EntryComponent,
      canActivate: [AuthGaurdService]
    },
    { path: '', 
      redirectTo: '', 
      pathMatch: 'full' },
      { path: '**', 
      component: LoginComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    EntryComponent,
    DocumentComponent,
    DocumentListComponent,
    MyLoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    DataTableModule,
    AngularDualListBoxModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers: [
    DatePipe,
    LoginService,
    LoaderService,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:LoaderInterceptorService,
      multi : true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
