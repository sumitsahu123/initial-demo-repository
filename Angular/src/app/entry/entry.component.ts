import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login/login.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-entry",
  templateUrl: "./entry.component.html",
  styleUrls: ["./entry.component.css"]
})
export class EntryComponent implements OnInit {
  groupName: String[];
  showRole: boolean = false;
  showDocument : boolean = false;
  constructor(public loginService: LoginService, private router: Router) {}

  ngOnInit() {
    let temp = localStorage.getItem("groupName");
    if(temp !== undefined || temp !== null){
      this.groupName = temp.split(",");
      this.showManage();
      this.router.navigate(["/entry"]);
    }else{
      this.router.navigate(["/"]);
    }

  }

  showManage(){
    if(this.groupName.includes("ROLE_HR_ADMINS") && this.groupName.includes("ROLE_FN_SMS")){
      this.showRole = true;
      this.showDocument = true;
    }else if(this.groupName.includes("ROLE_HR_ADMINS")){
      this.showDocument = true;
    }else if(this.groupName.includes("ROLE_FN_SMS")){
      this.showRole = true;
    }
  }

  roleManagement(){
    this.router.navigate(["/addDocument"]);
  }

  docManagement(){
    this.router.navigate(["/documentList"]);
  }

}
