import { Component, OnInit } from "@angular/core";
import { JwtRequest } from "./login";
import { LoginService } from "./login.service";
import { Router } from "@angular/router";
import { summaryForJitFileName } from "@angular/compiler/src/aot/util";


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: [
    "./login.component.css",
    "./../../../node_modules/font-awesome/css/font-awesome.css"
  ]
})
export class LoginComponent implements OnInit {
  // login: JwtRequest = new JwtRequest();
  username : string;
  password : string;
  timer : number;
  errorMsg: String = null;
  groupName: string[];
  constructor(private logonService: LoginService, private router: Router) {}

  ngOnInit() {
    this.username = localStorage.getItem("username");
    if (this.username != null){
      this.router.navigate(["/entry"])
    }
    if (this.username === null || this.username === ""){
      this.router.navigate(["/"])
    }
  }

  logon() {

    localStorage.setItem("username" , this.username);
    // let tokenStr = "Bearer " + userData.jwttoken;
    localStorage.setItem('token', this.username);
    // sessionStorage.setItem("token", tokenStr);
    const time_to_login = Date.now() + 9; // 15 minutes
    // sessionStorage.setItem("timer", JSON.stringify(time_to_login));
    localStorage.setItem("timer", JSON.stringify(time_to_login));
    // sessionStorage.setItem("groupName", userData.Group_Name);
    // console.log(userData);
    localStorage.setItem("groupName", "ROLE_HR_ADMINS,ROLE_FN_SMS");
    this.router.navigate(["/entry"]);
    console.log("logon called");
    // this.logonService
    //   .authenticate(this.username, this.password)
    //   .subscribe(
    //     data => {
    //       console.log(data);
    //       this.router.navigate(["/entry"]);
    //       // location.reload();
    //     },
    //     err => {
    //         this.errorMsg = Error(err).message;
    //     }
    //   );
      // this.logonService.login(this.login.username);

  }

  /*logon() {
    this.logonService.login(this.login).subscribe(
      data => {
        console.log(data);
        //this.router.navigate(["/role"]);
        this.router.navigate(["/role"]);
      },
      error => {
        console.log(error);
        this.errorMsg = error;
      }
    );
  }*/
}
