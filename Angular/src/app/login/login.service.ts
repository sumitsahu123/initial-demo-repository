import { Injectable, HostListener } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";

export class User {
  constructor(public status: string) {}
}

export class JwtResponse {
  constructor(public jwttoken: string) {
    
  }
}

@Injectable({
  providedIn: "root"
})

@Injectable()
export class LoginService {
  //private baseUrl = "https://10.10.19.55:636/logon";

  constructor(private httpClient: HttpClient, private router: Router) {}

  authenticate(username, password) {

    localStorage.setItem("username" , username);
    // let tokenStr = "Bearer " + userData.jwttoken;
    localStorage.setItem('token', username);
    // sessionStorage.setItem("token", tokenStr);
    const time_to_login = Date.now() + 9; // 15 minutes
    // sessionStorage.setItem("timer", JSON.stringify(time_to_login));
    localStorage.setItem("timer", JSON.stringify(time_to_login));
    // sessionStorage.setItem("groupName", userData.Group_Name);
    // console.log(userData);
    localStorage.setItem("groupName", "ROLE_HR_ADMINS,ROLE_FN_SMS");
    console.log("logon called");

    return this.httpClient
    .get(window.location.origin+`/navigator/jaxrs/logon.do/?userid=${username}&password=${password}`,{responseType:"text"})
    .pipe(map(userData => {
      // console.log(userData);
      var responseText = JSON.parse(userData.substr(4));
      // // {console.log(JSON.stringify(userData));
      console.log(responseText.userid);
      if(responseText.userid){
       
      }else{
        throw Error(responseText.errors[0].text);
        
      }
      location.reload();}));
    // localStorage.setItem("username" , username);
    // // let tokenStr = "Bearer " + userData.jwttoken;
    // localStorage.setItem('token', username);
    // // sessionStorage.setItem("token", tokenStr);
    // const time_to_login = Date.now() + 9; // 15 minutes
    // // sessionStorage.setItem("timer", JSON.stringify(time_to_login));
    // localStorage.setItem("timer", JSON.stringify(time_to_login));
    // // sessionStorage.setItem("groupName", userData.Group_Name);
    // // console.log(userData);
    // localStorage.setItem("groupName", "ROLE_HR_ADMINS");
    // location.reload();

    // return this.httpClient
    //   .post<any>("http://localhost:8081/api/authenticate", {
    //     username,
    //     password
    //   })
    //   .pipe(
    //     map(userData => {
    //       // sessionStorage.setItem("username", username);
    //       localStorage.setItem("username" , username);
    //       // let tokenStr = "Bearer " + userData.jwttoken;
    //       localStorage.setItem('token', username);
    //       // sessionStorage.setItem("token", tokenStr);
    //       const time_to_login = Date.now() + 9; // 15 minutes
    //       // sessionStorage.setItem("timer", JSON.stringify(time_to_login));
    //       localStorage.setItem("timer", JSON.stringify(time_to_login));
    //       // sessionStorage.setItem("groupName", userData.Group_Name);
    //       console.log(userData);
    //       localStorage.setItem("groupName", "ROLE_HR_ADMINS");
    //       location.reload();
    //       return userData;
    //     })
    //   );
  }

  isUserLoggedIn() {
    // location.reload();
    let user = localStorage.getItem("token");
    //console.log(!(user === null))
    return !(user === null);
  }

  logout() {
    this.httpClient.get(window.location.origin+`/navigator/jaxrs/logoff`,{responseType:"text"}).subscribe();
    this.router.navigate(["/"]);
    let username = localStorage.getItem("username");
    console.log(username);
    localStorage.removeItem("token");
    localStorage.removeItem("timer");
    localStorage.removeItem("username");
    localStorage.removeItem("groupName");
    // location.reload();
    
  }

  logoutTime(username){
      this.httpClient.post<any>(`"https://10.240.20.22:9443/kotak/api/logoutTime/"`, username);
  }

  @HostListener("window:beforeunload", ["$event"])
  public handleBeforeUnload(event) {
    this.logout();
  }

  /*login(login: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, login);
  }
*/
}
