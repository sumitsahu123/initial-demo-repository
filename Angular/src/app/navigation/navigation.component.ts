import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login/login.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.css"]
})
export class NavigationComponent implements OnInit {
  groupName: string;
  username : string;

  constructor(public loginService: LoginService, private router: Router) {
    this.username = localStorage.getItem("username");
  }

  ngOnInit() {

    // location.reload();
    // console.log(this.username);

  }

  logout() {
    this.loginService.logout();
    this.router.navigate(["/"]);
    // location.reload();
  }

}
